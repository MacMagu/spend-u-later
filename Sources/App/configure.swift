import FluentPostgreSQL
import Vapor
public func configure(
	_ config: inout Config,
	_ env: inout Environment,
	_ services: inout Services
	) throws {
	try services.register(FluentPostgreSQLProvider())
	let router = EngineRouter.default()
	try routes(router)
	services.register(router, as: Router.self)
	var middlewares = MiddlewareConfig()
	middlewares.use(ErrorMiddleware.self)
	services.register(middlewares)
	
	// Configure the database
	var databases = DatabasesConfig()
	
	let databaseConfig = PostgreSQLDatabaseConfig(
		hostname: "localhost",
		username: "vapor",
		database: "vapor",
		password: "password")
	let database = PostgreSQLDatabase(config: databaseConfig)
	databases.add(database: database, as: .psql)
	services.register(databases)
	var migrations = MigrationConfig()
	
	//make sure that parents are created before objects that have a child relationship with them
	migrations.add(model: User.self, database: .psql)
	migrations.add(model: Event.self, database: .psql)
	migrations.add(model: Expense.self, database: .psql)
	migrations.add(model: EventUserPivot.self, database: .psql)
	services.register(migrations)
}

