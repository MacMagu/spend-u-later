import Vapor
import FluentPostgreSQL

final class Event: Codable {
	
	var id: Int?
	var name: String
	var createdBy: String
	
	init(name: String, createdBy: String) {
		self.name = name
		self.createdBy = createdBy
	}
}

extension Event: PostgreSQLModel {}

extension Event: Migration {}

extension Event: Content {}

extension Event: Parameter {}

extension Event {
	
	var expenses: Children<Event, Expense> {
		return children(\.eventID)
	}
	
	var users: Siblings<Event, User, EventUserPivot> {
		return siblings()
		
	}
}
