import Vapor
import FluentPostgreSQL

final class Expense: Codable {
    
    var id: Int?
    var name: String
    var description: String
    var userID: User.ID
    var incurredBy: User.ID
    var portionOfOriginal: Float
    var eventID: Event.ID
    
    init(name: String, description: String, userID: User.ID, incurredBy: User.ID, portionOfOriginal: Float, eventID: Event.ID) {
        self.name = name
        self.description = description
        self.userID = userID
        self.incurredBy = incurredBy
        self.portionOfOriginal = portionOfOriginal
        self.eventID = eventID
    }
}

extension Expense: PostgreSQLModel {}

extension Expense: Migration {}

extension Expense: Content {}

extension Expense: Parameter {}

extension Expense {
	
    var event: Parent<Expense, Event> {
        return parent(\.eventID)
    }
    
    var user: Parent<Expense, User> {
        return parent(\.userID)
    }
}
