import FluentPostgreSQL
import Foundation

final class EventUserPivot: PostgreSQLUUIDPivot {
	var id: UUID?
	var eventID: Event.ID
	var userID: User.ID
	
	
	typealias Left = Event
	typealias Right = User
	
	static let leftIDKey: LeftIDKey = \.eventID
	static let rightIDKey: RightIDKey = \.userID
	
	init(_ event: Event,_ user: User) throws {
		self.eventID = try event.requireID()
		self.userID = try user.requireID()
	}
	
}

extension EventUserPivot: Migration {}
extension EventUserPivot: ModifiablePivot {}
