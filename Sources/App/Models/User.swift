import Vapor
import FluentPostgreSQL

final class User: Codable {
	
	var id: UUID?
	var name: String
	
	init(name: String) {
		self.name = name
	}
}

extension User: PostgreSQLUUIDModel {}

extension User: Migration {}

extension User: Content {}

extension User: Parameter {}

extension User {
	var expenses: Children<User, Expense> {
		return children(\.userID)
	}
	
	var events: Siblings<User, Event, EventUserPivot> {
		return siblings()
	}
}
