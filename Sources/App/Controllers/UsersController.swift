import Vapor
import Fluent

struct UsersController: RouteCollection {
	func boot(router: Router) throws {
		
		let usersRoutes = router.grouped("api", "users")
		
		usersRoutes.post(User.self, use: createHandler)
		usersRoutes.get(use: getAllHandler)
		usersRoutes.get(User.parameter, use: getHandler)
		usersRoutes.get(Expense.parameter, "expenses", use: getExpensesHandler)
		usersRoutes.get(User.parameter, "events", use: getEventsHandler)
	}
	
	func createHandler(_ req: Request, user: User) throws -> Future<User> {
		return user.save(on: req)
	}
	
	func getAllHandler(_ req: Request) throws -> Future<[User]> {
		return User.query(on: req).all()
	}
	func getHandler(_ req: Request) throws -> Future<User> {
		return try req.parameters.next(User.self)
	}
	
	func getExpensesHandler(_ req: Request) throws -> Future<[Expense]> {
		return try req.parameters.next(User.self)
			.flatMap(to: [Expense].self) { user in
				try user.expenses.query(on: req).all()
		}
	}
	
	func getEventsHandler(_ req: Request) throws -> Future<[Event]> {
		return try req.parameters.next(User.self)
			.flatMap(to: [Event].self) { user in
				try user.events.query(on: req).all()
		}
	}
}

