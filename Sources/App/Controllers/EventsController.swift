import Vapor
import Fluent

struct EventsController: RouteCollection {
    func boot(router: Router) throws {
		
		let eventsRoutes = router.grouped("api", "events")
		
		eventsRoutes.post(Event.self, use: createHandler)
        eventsRoutes.get(use: getAllHandler)
        eventsRoutes.get(Event.parameter, "users", use: getUsersHandler)
        eventsRoutes.get(Event.parameter, "expenses", use: getExpensesHandler)
        eventsRoutes.post(Event.parameter, "users", User.parameter, use: addUserHandler)
        eventsRoutes.delete(Event.parameter, "users", User.parameter, use: removeUserHandler)
}
    
    func createHandler(_ req: Request, event: Event) throws -> Future<Event> {
        return event.save(on: req)
    }
    
    func getAllHandler(_ req: Request) throws -> Future<[Event]> {
        return Event.query(on: req).all()
    }
    
    func getUsersHandler(_ req: Request) throws -> Future<[User]> {
            return try req.parameters.next(Event.self)
                .flatMap(to: [User].self) { event in
                    try event.users.query(on: req).all()
            }
    }
    
    func getExpensesHandler( req: Request) throws -> Future<[Expense]> {
        return try req.parameters.next(Event.self)
            .flatMap(to: [Expense].self) { event in
                try event.expenses.query(on: req).all()
        }
    }
    
    func addUserHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try flatMap(
            to: HTTPStatus.self,
            req.parameters.next(Event.self),
            req.parameters.next(User.self)) { event, user in
                return event.users
                    .attach(user, on: req)
                    .transform(to: .created)
        }
    }
    
    func removeUserHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try flatMap(
            to: HTTPStatus.self,
            req.parameters.next(Event.self),
            req.parameters.next(User.self)) { event, user in
                return event.users
                    .detach(user, on: req)
                    .transform(to: .noContent)
        }
    }
    
}
