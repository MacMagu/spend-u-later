import Vapor
import Fluent

struct ExpensesController: RouteCollection {
	func boot(router: Router) throws {
		
		let expensesRoutes = router.grouped("api", "expenses")
		
		expensesRoutes.get(use: getAllHandler)
		expensesRoutes.post(Expense.self, use: createHandler)
		expensesRoutes.put(Expense.parameter, use: updateHandler)
		expensesRoutes.get(Expense.parameter, "user", use: getUserHandler)
		expensesRoutes.get(Expense.parameter, "event", use: getEventHandler)
		expensesRoutes.get("search", use: getPersonalExpenses)
	}
	
	func getAllHandler(_ req: Request) throws -> Future<[Expense]> {
		return Expense.query(on: req).all()
	}
	
	func createHandler(_ req: Request, expense: Expense) throws -> Future<Expense> {
		return expense.save(on: req)
	}
	
	func updateHandler(_ req: Request) throws -> Future<Expense> {
		return try flatMap(
			to: Expense.self,
			req.parameters.next(Expense.self),
			req.content.decode(Expense.self)
		) { expense, updatedExpense in
			expense.name = updatedExpense.name
			expense.description = updatedExpense.description
			expense.userID = updatedExpense.userID
			expense.incurredBy = updatedExpense.incurredBy
			expense.portionOfOriginal = updatedExpense.portionOfOriginal
			expense.eventID = updatedExpense.eventID
			return expense.save(on: req)
		} }
	
	func getUserHandler(_ req: Request) throws -> Future<User> {
		return try req.parameters.next(Expense.self)
			.flatMap(to: User.self) { expense in
				expense.user.get(on: req)
		}
	}
	
	func getEventHandler(_ req: Request) throws -> Future<Event> {
		return try req.parameters.next(Expense.self)
			.flatMap(to: Event.self) { expense in
				expense.event.get(on: req)
		}
	}
	
	func getPersonalExpenses(_ req: Request) throws -> Future<[Expense]> {
		guard let searchTerm = req.query[String.self, at: "user"], let userID = User.ID(searchTerm) else {
			throw Abort(.badRequest)
		}
		return Expense.query(on: req)
			.filter(\.incurredBy == userID)
			.all()
		
	}
	
}
