import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
	let usersController = UsersController()
	try router.register(collection: usersController)
	let expenseController = ExpensesController()
	try router.register(collection: expenseController)
	let eventsController = EventsController()
	try router.register(collection: eventsController)
}

